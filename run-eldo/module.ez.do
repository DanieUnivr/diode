############################################################
## EZwave - Saved Window File
## Thursday, March 17, 2022 at 12:49:53 PM UTC
##
## Note: This is an auto-generated file.
##
## In case of modification, Do not remove this comment
############################################################

onerror {resume}

# ===== Open required Database =====
dataset open /home/user/Desktop/diode/run-eldo/module.wdb module

# ===== Open the window =====
wave addwindow -x 0  -y 0 -width 1155  -height 615 -divider 0.81

# ===== Create row #1 =====
add wave  -show TRAN.v -color -16711936 -separator : -terminals  :module:ytop:C1:(B,C)


# ===== Create row #2 =====
add wave  -show TRAN.v -color -256 -separator : -terminals  :module:ytop:C1:(C,gnd)


# ===== Create row #3 =====
add wave  -show TRAN.v -color -16744193 -separator : -terminals  :module:ytop:C1:(volt,B)


# ===== Create row #4 =====
add wave  -show TRAN.v -color -3650716 -separator : -terminals  :module:ytop:C1:volt


# ===== Set X-axis min/max extents =====
wave zoomrange  3.75 6.25

# ====== Create the cursors, markers and measurements =====
