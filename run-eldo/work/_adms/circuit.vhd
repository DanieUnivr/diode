LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

LIBRARY STD;
USE STD.ALL;

ENTITY circuit IS
  GENERIC (
   R0 :   integer  := 500;
   \IS\ :   real :=  1.000000e-14;
   r :   integer  := 100 );
  PORT (
    SIGNAL volt : INOUT STD_LOGIC;
    SIGNAL gnd : INOUT STD_LOGIC;
    SIGNAL B : INOUT STD_LOGIC;
    SIGNAL C : INOUT STD_LOGIC);
END ENTITY circuit;

