library ieee;
use ieee.std_logic_1164.all;

use ieee.upf.all;

entity circuit is
  generic (
   R0 :   integer  := 500;
   \IS\ :   real :=  1.000000e-14;
   r :   integer  := 100 );
  PORT (
    signal volt : INOUT std_logic;
    signal gnd : INOUT std_logic;
    signal B : INOUT std_logic;
    signal C : INOUT std_logic);
end entity circuit;

