library ieee;
use ieee.std_logic_1164.all;

use ieee.upf.all;

entity diode is
  generic (
   \IS\ :   real :=  1.000000e-14;
   r :   integer  := 0 );
  PORT (
    signal a : INOUT std_logic;
    signal c : INOUT std_logic);
end entity diode;

